FROM rails:onbuild

CMD bundle exec bin/rake db:create db:migrate && bundle exec bin/rails server puma
