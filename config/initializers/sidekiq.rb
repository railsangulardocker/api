Sidekiq.configure_server do |config|
    config.redis = { url: ENV['REDIS_PORT_6379_TCP'] }
end

Sidekiq.configure_client do |config|
    config.redis = { url: ENV['REDIS_PORT_6379_TCP'] }
end
